import { readFile, writeFile } from 'fs';
import { filePath } from '../config.js';
import { replyToUser } from './bot.js';

let usersCache = [];

export const getUsers = async() => {
    try {
        if(usersCache && usersCache.length){
            return usersCache;
        }
        const {users} = await readUsersFile();
        usersCache = users;
    } catch(error) {
        if (error.code !== 'ENOENT') {
            throw error;
        }
    }
    return usersCache;
}

export async function saveUser(userId) {
    const users = await getUsers();
    if (users.includes(userId)) {
        replyToUser(userId, 'You are already on the list');
    } else {
        usersCache.push(userId);
        saveUsersFile();
        replyToUser(userId, 'You are now on the list');
    }
}

export async function deleteUser(userId) {
    const users = await getUsers();
    const userPosition = users.indexOf(userId);
    if (!(userPosition + 1)) {
        replyToUser(userId, 'You was not on the list');
    } else {
        users.splice(userPosition, 1);
        saveUsersFile();
        replyToUser(userId, 'You was removed from the list');
    }
}

/**
 * @return {Promise<{users: number[]}>}
 */
export async function readUsersFile() {
    return new Promise((resolve, reject) => {
        try {
            readFile(filePath, 'utf8', (error, jsonData) => {
                if (error) throw error;
                const contactsList = JSON.parse(jsonData);
                resolve(contactsList);
            });
            
        } catch(error) {
            reject(error);
        }
    })
}

function saveUsersFile() {
    try {
        let contactsList = {};
        contactsList.users = usersCache;
        const jsonData = JSON.stringify(contactsList, null, 2);  
        writeFile(filePath, jsonData, error => {
            if (error) throw error;
        });
    } catch(error) {
        throw error;
    }
}