process.env["NTBA_FIX_319"] = 1;

import TelegramBot from 'node-telegram-bot-api';
import { deleteUser, saveUser } from './user.js';

const token = '591503575:AAEFk6l5ICdgpsmOKipMrak1mDh6v145M4o';

const botHelp = `Wellcome to Jenkins-bot, the commands list:
                \n/add - to add yourself to the contact list.
                \n/delete - to delete yourself from the contact list.
                \n/help - to get this message again.`; 

const bot = new TelegramBot(token, {polling: true});

bot.onText(/\/add/, msg => {
    const userId = msg.chat.id;
    saveUser(userId);
});

bot.onText(/\/delete/, msg => {
    const userId = msg.chat.id;
    deleteUser(userId);
});

bot.onText(/\/start/, msg => {
    const userId = msg.chat.id;
    bot.sendMessage(userId, botHelp);
});

bot.onText(/\/help/, msg => {
    const userId = msg.chat.id;
    bot.sendMessage(userId, botHelp);
});

bot.on('polling_error', (error) => {
    console.log(error.code);
});
                
export const replyToUser = (userId, responseString) => {
    bot.sendMessage(userId, responseString);
}