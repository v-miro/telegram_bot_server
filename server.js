process.env["NTBA_FIX_319"] = 1;
import { createServer } from 'http';
import { parse } from 'url';
import { port } from './config.js';
import { replyToUser } from './src/bot.js';
import { getUsers } from './src/user.js';

const server = createServer();
server.setTimeout(1000);

server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }
    console.log(`Server is listening on ${port}`);
});

server.on('request', (request, response) => {
    const data = parse(request.url,true).query;
    
    if (data.stopServer) {
        server.close(() => {
            process.exit(0);
        });
        response.end('Your request was recieved');
        return false;
    }

    const responseString = 'Error in ' + data.buildName + '\nUrl: ' + data.buildUrl + '\nAdditional info: ' + data.addInfo;
    notifyUsers(responseString);
    response.end('Your request was recieved');
});

async function notifyUsers(responseString) {
    const users = await getUsers();
    users.forEach(userId => {
        replyToUser(userId, responseString);
    });
}