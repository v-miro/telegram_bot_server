const path = require('path');
 
module.exports = {
    mode: 'development',
    // node: {
    //     fs: 'empty',
    // },
    target: 'node',
    // devtool: false,
    entry: ['babel-polyfill', './server.js'],
    output: {
        globalObject: 'this',
        filename: 'server.js',
        path: path.resolve(__dirname, 'app')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }
        ]
    }
};



